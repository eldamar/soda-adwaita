# Adwaita Soda (Gnome 3 sublime text theme)

This is sublime text theme based on Soda to match the Gnome 3 (Adwaita Theme) forked from [johonunu](https://bitbucket.org/johonunu/soda-adwaita)

## Screenshot
![Screenshot](https://bitbucket.org/eldamar/soda-adwaita/raw/d042cb734819ce65fcb55f4748772a8ba285e96b/adwaita.png)

## Installation

### Using Git

If you are a git user, you can install the theme and keep up to date by cloning the repo directly into your `Packages` directory in the Sublime Text 2 application settings area.

You can locate your Sublime Text 2 `Packages` directory by using the menu item `Preferences -> Browse Packages...`.

While inside the `Packages` directory, clone the theme repository using the command below:

    git clone https://bitbucket.org/eldamar/soda-adwaita.git "Theme - Soda Adwaita"

### Download Manually

* Download the files using the GitHub .zip download option
* Unzip the files and rename the folder to `Theme - Soda`
* Copy the folder to your Sublime Text 2 `Packages` directory

## Activating the theme

To configure Sublime Text 2 to use the theme:

* Open your User Settings Preferences file `Sublime Text 2 -> Preferences -> Settings - User`
* Add (or update) your theme entry to be `"theme": "Soda Adwaita.sublime-theme"`

### Example User Settings

    {
        "theme": "Soda Adwaita.sublime-theme",
        "soda_classic_tabs": true
    }


